const themeButton = document.getElementById('theme-button');
let currentTheme = localStorage.getItem('theme') || 'light';

function setTheme(theme) {
    if (theme === 'dark') {
        document.body.style.backgroundColor = '#333';
        themeButton.style.backgroundColor = '#f0f0f0';
        changeFontColor('#fff');
    } else {
        document.body.style.backgroundColor = '#f0f0f0';
        themeButton.style.backgroundColor = '#3498db';
        changeFontColor('#333');
    }
    localStorage.setItem('theme', theme);
}

function changeFontColor(color) {
    const textElements = document.querySelectorAll('p, a, b, span, section, class, div, h1, h2, h3, h4, h5, h6');
    textElements.forEach((element) => {
        element.style.color = color;
    });
}

setTheme(currentTheme);

themeButton.addEventListener('click', () => {
    if (currentTheme === 'dark') {
        setTheme('light');
        currentTheme = 'light';
    } else {
        setTheme('dark');
        currentTheme = 'dark';
    }
});